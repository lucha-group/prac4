#include <iostream>
#include <fstream>
#include <vector>
#include <initializer_list>
#include <type_traits>

using namespace std;


class Vector3D {
private:
    int X;
    int Y;
    int Z;
public:
    Vector3D(initializer_list<int> list) {
        if (list.size() == 3) {
            X = *(list.begin());
            Y = *(list.begin() + 1);
            Z = *(list.begin() + 2);
        }
    }

    int getX() {
        return X;
    }

    int getY() {
        return Y;
    }

    int getZ() {
        return Z;
    }

    void setX(int x) {
        X = x;
    }

    void setY(int y) {
        Y = y;
    }

    void setZ(int z) {
        Z = z;
    }


    virtual void info() {
        cout << "X = " << X << " Y = " << Y << " Z = " << Z << endl;
    }

    virtual ~Vector3D() {
        //cout << "Vector3D deleted" << endl;
    }
};


class Vector4D : public Vector3D {
private:
    int W;
public:
    Vector4D(initializer_list<int> list) : Vector3D({*(list.begin()), *(list.begin() + 1), *(list.begin() + 2)}) {
        if (list.size() == 4) {
            W = *(list.begin() + 3);
        }
        else W = 0;
    }

    int getW() {
        return W;
    }

    void setW(int w) {
        W = w;
    }

    void info() {
        cout << "X = " << getX() << " Y = " << getY() << " Z = " << getZ() << " W = " << W << endl;
    }

    ~Vector4D() {
        //cout << "Vector4D deleted" << endl;
    }
};

template<class type = Vector4D, int size = 0>
class Manifold {
    static_assert(is_base_of<Vector3D, type>::value,
                  "It is not a derived class");
private:
    int n = 0;
    vector<type> arr;
public:
    void Add(initializer_list<int> list) {

        if (n < size) {
            type v(list);
            arr.push_back(v);
            n++;
        } else {
            cout << " Container is full" << endl;
        }
    }

    void List() {
        for (int i = 0; i < n; i++)
            arr[i].info();
    }
};


int main() {
    Manifold<Vector4D, 5> v2;
    v2.Add({1, 2, 3, 4});
    v2.Add({2, 2, 3, 4});
    v2.Add({3, 2, 3, 4});
    v2.Add({4, 2, 3, 4});
    v2.Add({5, 2, 3, 4});
    v2.Add({6, 2, 3, 4});
    v2.List();
    return 0;
}
